package com.example.quizzkt.dao

import android.provider.BaseColumns

object DbContract {

    class ThemeEntry : BaseColumns {
        companion object {
            val TABLE_NAME = "THEME"
            val COLUMN_ID = "ID_THEME"
            val COLUMN_LIBELLE = "LIBELLE_THEME"
        }
    }

    class QuestionEntry : BaseColumns {
        companion object {
            val TABLE_NAME = "QUESTION"
            val COLUMN_ID = "ID_QUESTION"
            val COLUMN_LIBELLE = "LIBELLE_QUESTION"
            val COLUMN_FK_THEME = "FK_THEME"
        }

    }

    class PropositionEntry : BaseColumns {
        companion object {
            val TABLE_NAME = "PROPOSITIONS"
            val COLUMN_ID = "ID_PROPOSITION"
            val COLUMN_LIBELLE = "LIBELLE_PROPOSITION"
            val COLUMN_ISCORRECT = "ISCORRECT"
            val COLUMN_FK_QUESTION = "FK_QUESTION"
        }
    }
}