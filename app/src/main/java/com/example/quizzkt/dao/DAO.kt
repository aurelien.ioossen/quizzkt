package com.example.quizzkt.dao

import android.content.Context
import android.database.Cursor
import com.example.quizzkt.job.Proposition
import com.example.quizzkt.job.Question
import com.example.quizzkt.utils.theme.Theme
import java.io.BufferedReader
import java.io.File
import java.lang.Exception
import kotlin.random.Random

class DAO(context: Context) {

    private var quizzDAO = MySQLite.getInstance(context)
    private var bdd = quizzDAO.readableDatabase


    fun getQuestion(theme: Theme): Question {
        var randomIdOfQuestion = Random.nextInt(1, 3)
//        var randomIdOfQuestion = 2


        var questionGenerated = Question(0, "", 0)
        var cursor: Cursor? = null
        var query: String =
            "SELECT * FROM QUESTIONS Q INNER JOIN PROPOSITIONS P ON Q.IDQUESTION = P.IDQUESTIONS WHERE Q.IDQUESTION = " + randomIdOfQuestion
        cursor = bdd.rawQuery(query, null)

        if (cursor.moveToFirst()) {
            try {
                var isFinish = false
                questionGenerated = Question(
                    cursor.getInt(0),
                    cursor.getString(1),
                    cursor.getInt(2)
                )
                questionGenerated.addProposition(
                    Proposition(
                        cursor.getInt(3),
                        cursor.getString(4),
                        cursor.getInt(5) > 0,
                        cursor.getInt(6)
                    )
                )




                while (!isFinish) {

                    if (!cursor.isLast) {
                        cursor.moveToNext()
                        questionGenerated.addProposition(
                            Proposition(
                                cursor.getInt(3),
                                cursor.getString(4),
                                cursor.getInt(5) > 0,
                                cursor.getInt(6)
                            )
                        )

                    } else {
                        isFinish = true
                    }
                }
            } catch (e: Exception) {
                println("ERREUR : " + e)
            }


        }
        println(questionGenerated)
        return questionGenerated
    }


    fun getAllTheme(): ArrayList<Theme> {

        var listTheme = ArrayList<Theme>()

        var cursor: Cursor? = null

        try {

            var isFinish = false
            cursor = bdd.rawQuery("SELECT * FROM " + DbContract.ThemeEntry.TABLE_NAME, null)
            if (cursor.moveToFirst()) {

                while (!isFinish) {

                    listTheme.add(changeCursorToTheme(cursor))
                    if (!cursor.isLast) {
                        cursor.moveToNext()
                    } else {
                        isFinish = true
                    }
                }

            }

        } catch (e: Exception) {

            println("ERREUR " + e)
        }
        println("Liste des thèmes :" + listTheme)
        return listTheme


    }


    fun changeCursorToTheme(cursor: Cursor): Theme {
        println("curseur :id" + cursor.getInt(0))
        println("curseur :nom" + cursor.getString(1))
        println("curseur :icone" + cursor.getString(2))
        println("curseur :libelle" + cursor.getString(3))

        return Theme(

            cursor.getInt(0),
            cursor.getString(1),
            cursor.getString(2),
            cursor.getString(3)

        )


    }


}