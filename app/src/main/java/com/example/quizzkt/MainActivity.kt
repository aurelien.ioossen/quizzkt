package com.example.quizzkt

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.quizzkt.dao.DAO
import com.example.quizzkt.utils.theme.Theme
import com.example.quizzkt.utils.theme.ThemeAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

var themeSelected = Theme(0,"","","")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var listTheme = generateTheme()
        var adapterTheme = ThemeAdapter(
            this,
            R.layout.theme_layout,
            listTheme
        )
        listViewTheme.adapter = adapterTheme
        listViewTheme.setOnItemClickListener({ parent, view, position, id ->
            themeSelected = parent.getItemAtPosition(position) as Theme
            lunchGame()


//            Toast.makeText(
//                this,
//                "Personne selectionnée : ${personneSelected.prenom.toUpperCase()}",
//                Toast.LENGTH_SHORT
//            ).show()
        }
        )
    }


    fun lunchGame(){

        var gameIntent = Intent(this,GameActivity::class.java)
        gameIntent.putExtra("themeSelected",themeSelected)
        startActivity(gameIntent)



    }

    fun generateTheme(): ArrayList<Theme> {
        var listTheme = ArrayList<Theme>()
        var dao = DAO(this)
        listTheme = dao.getAllTheme()


//        listTheme.add(
//            Theme(1,
//                "Mathématiques",
//                "maths",
//                "Cosinus,sinus..."
//            )
//        )
//        listTheme.add(
//            Theme(2,
//                "Histoire",
//                "histoire",
//                "frrfrf..."
//            )
//        )

        return listTheme
    }
}
