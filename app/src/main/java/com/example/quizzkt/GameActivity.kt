package com.example.quizzkt

import android.graphics.Color.GREEN
import android.graphics.Color.RED
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import com.example.quizzkt.dao.DAO
import com.example.quizzkt.job.Proposition
import com.example.quizzkt.job.Question
import com.example.quizzkt.utils.theme.Theme
import kotlinx.android.synthetic.main.game_layout.*

class GameActivity : AppCompatActivity(), View.OnClickListener {
    var themeSelected = Theme(0, "", "", "")
    var dao = DAO(this)
    var score: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.game_layout)
        themeSelected = intent.getParcelableExtra<Theme>("themeSelected")


        generateQuestion()

    }


    fun generateQuestion() {

        var question = dao.getQuestion(themeSelected)
        println("QUESTION  :${question.libelle}")
        tvQuestion.text = question.libelle
        listViewProposition.adapter = ArrayAdapter<Proposition>(
            this,
            android.R.layout.simple_list_item_1,
            question.listProposition
        )
        listViewProposition.setOnItemClickListener { parent, view, position, id ->
            var propositionSelected = parent.getItemAtPosition(position) as Proposition
            checkAnswer(propositionSelected)

        }
    }


    fun checkAnswer(proposition: Proposition) {
        if (proposition.isCorrect) {
            tvAnswer.setTextColor(GREEN)
            tvAnswer.text = "BONNE REPONSE !"
            score += 10

        } else {
            tvAnswer.setTextColor(RED)
            tvAnswer.text = "FAUX !"
            score -= 5
        }

        tvScore.text = score.toString()
        generateQuestion()


    }

    override fun onClick(v: View?) {


    }
}