package com.example.quizzkt.job

class Question(var id: Int, var libelle: String, var idTheme: Int) {
    var listProposition = ArrayList<Proposition>()


    fun setListOfProposition(listProposition: ArrayList<Proposition>) {

        this.listProposition = listProposition

    }

    fun addProposition(proposition: Proposition) {

        listProposition.add(proposition)

    }

    fun getListOfProposition(): ArrayList<Proposition> {
        return listProposition
    }

    override fun toString(): String {
        return "Question(id=$id, libelle='$libelle', idTheme=$idTheme, listProposition=$listProposition)"
    }


}