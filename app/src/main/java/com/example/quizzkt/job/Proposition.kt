package com.example.quizzkt.job

data class Proposition(var id:Int,var libelle:String,var isCorrect:Boolean,var idQuestion:Int) {

    override fun toString(): String {
        return "Proposition(id=$id, libelle='$libelle', isCorrect=$isCorrect, idQuestion=$idQuestion)"
    }
}