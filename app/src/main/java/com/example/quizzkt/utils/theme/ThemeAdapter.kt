package com.example.quizzkt.utils.theme

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.example.quizzkt.R
import com.example.quizzkt.utils.theme.Theme

class ThemeAdapter( context: Context, resource: Int, themes: ArrayList<Theme>):ArrayAdapter<Theme>(context, resource,themes) {
    val themes = themes




    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var view = convertView
        if (convertView == null) {

            var layoutInflater =
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = layoutInflater.inflate(R.layout.theme_layout, parent, false)

            var theme = themes.get(position)
            var tvTitre = view.findViewById<TextView>(R.id.themeTitle)
            var ivIcone = view.findViewById<ImageView>(R.id.themeIcone)
            var tvLibelle = view.findViewById<TextView>(R.id.tvLibelle)

            var iconeId =
                view.resources.getIdentifier(theme.icone, "drawable", "com.example.quizzkt")
            val drawableIcone = ContextCompat.getDrawable(context, iconeId)

            ivIcone.setImageDrawable(drawableIcone)
            tvTitre.text = theme.nom
            tvLibelle.text = theme.libelle


        }

        return view!!
    }
}