package com.example.quizzkt.utils.theme

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
data class Theme(var id:Int,var nom:String,var icone:String,var libelle:String):Parcelable {

}